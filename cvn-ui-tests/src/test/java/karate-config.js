function fn() {
    var env = karate.env; // get system property 'karate.env'
    var driverConfig = { type: 'chrome', showDriverLog: true, start: false }; //Set start flag to true before running with docker in jenkin context
    karate.configure('driver', driverConfig);
    karate.log('karate.env system property was:', env);
    if (!env) {
        env = 'uat';
    }
    var config = {
        env: env,
        appUsername: java.lang.System.getenv().getOrDefault("APP_USERNAME", "SA_JMETERTESTS"),
        appPassword: java.lang.System.getenv().getOrDefault("APP_PASSWORD", "datst2376"),
        DELAY_1X: 1000,
        DELAY_2X: 2000,
        DELAY_3X: 3000,
        baseUrl: 'http://sbuat1.intra.schneider.com/'
    }
    if (env == 'unt') {
        // customize
        // e.g. config.foo = 'bar';
        config.baseUrl = 'http://sbunt1.intra.schneider.com/';
    } else if (env == 'fit') {
        // customize
        config.baseUrl = 'http://sbfit1.intra.schneider.com/';
    } else if (env == 'uat') {
        // customize
        config.baseUrl = 'http://sbuat1.intra.schneider.com/';
    }
    return config;
}
