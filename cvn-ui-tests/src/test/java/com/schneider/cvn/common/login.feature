Feature: Login to schneider application portal

  Background:
    * configure driver = { type: 'chrome', showDriverLog: true }
    * def ANHome = baseUrl + 'accessorialnotification/ui/faces/jsps/accessorialnotificationhomepage.jspx'
    * call read ('this:locators.json')

  Scenario: Login to application portal & validate the Accessorial Notification home page
      Given driver ANHome
        And driver.maximize()
        And input(loginPage.userName, appUsername)
        And input(loginPage.password, appPassword)
        And waitFor(loginPage.logOnButton)
       When submit().click(loginPage.logOnButton)
       Then waitForUrl(ANHome)
